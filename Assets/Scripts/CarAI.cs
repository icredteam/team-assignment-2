﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarAI : MonoBehaviour {

    // neighborhood is the distance from the center of intersection to start turn
    public float neighborhood, speed, rightTurnRotation, leftTurnRotation;

    // holds the location of the center of each intersection
    private Vector3[] intersections = { new Vector3(88, 0, -68), new Vector3(0, 0, -68), new Vector3(-88, 0, -68), new Vector3(88, 0, 20), new Vector3(0, 0, 20), new Vector3(-88, 0, 20), new Vector3(88, 0, 108), new Vector3(0, 0, 108), new Vector3(-88, 0, 108) };

    private bool decisionMade = false, audioPlaying = false, northGreen = true, eastGreen = false, yellow = false;

    // goal is the rotation in the world each car should be facing
    private int rand = 2, goal;

    // only way I could get left turn to work, handles the case of rotation being 0
    private float leftTurn, rightTurn, time, waitTime;

    // last intersection the car visited, used to determine when we are far enough away to make next decision
    private Vector3 lastIntersection;

    // used for sounds
    private GameObject audio;
    private GameObject[] cars, badCars;
    void Start() {

        audio = GameObject.Find("AICarAudio");
        badCars = GameObject.FindGameObjectsWithTag(this.gameObject.tag);
        cars = new GameObject[badCars.Length - 1];
        for (int i = 0, index = 0; i < badCars.Length; i++) {
            if (badCars[i] == this.gameObject)
                continue;

            cars[index] = badCars[i];
            index++;
        }
        goal = (int)this.transform.eulerAngles.y;
        time = 5;
    }

    void Update() {

        int orientation = Mathf.RoundToInt(transform.eulerAngles.y);

        // handles the traffic lights
        time -= Time.deltaTime;
        if (time <= 0 && northGreen) {
            yellow = true;
            waitTime -= Time.deltaTime;
            if (waitTime <= 0) {
                time = 5;
                waitTime = 3;
                northGreen = false;
                eastGreen = true;
                yellow = false;
            }
        }
        else if (time <= 0 && eastGreen) {
            yellow = true;
            waitTime -= Time.deltaTime;
            if (waitTime <= 0) {
                time = 5;
                waitTime = 3;
                northGreen = true;
                yellow = false;
                eastGreen = false;
            }
        }

        // finds what intersection the car is at and determines if it is a valid time to make a decison on turning
        if (Vector3.Distance(intersections[0], transform.position) < neighborhood && !decisionMade) {

            lastIntersection = intersections[0];
            decisionMade = true;
            audioPlaying = false;

            // right turn only
            if (orientation == 0) {

                rand = 0;
                goal += 90;
                goal %= 360;
                rightTurn = transform.eulerAngles.y;
            }
            // left turn only
            else {

                rand = 1;
                if (goal == 0) goal = 270;
                else goal -= 90;
                goal = goal % 360;
                leftTurn = transform.eulerAngles.y;
            }
        }
        else if (Vector3.Distance(intersections[1], transform.position) < neighborhood && !decisionMade) {

            if (((orientation == 0 || orientation == 180) && northGreen) || ((orientation == 90 || orientation == 270) && eastGreen) && !yellow) {

                rand = Mathf.RoundToInt(Random.Range(0, 2));

                lastIntersection = intersections[1];
                decisionMade = true;
                audioPlaying = false;

                // right or left
                if (orientation == 0) {
                    if (rand == 0) {

                        goal += 90;
                        goal = goal % 360;
                        rightTurn = transform.eulerAngles.y;
                    }
                    else if (rand == 1) {

                        if (goal == 0) goal = 270;
                        else goal -= 90;
                        goal = goal % 360;
                        leftTurn = Mathf.RoundToInt(transform.eulerAngles.y);
                    }
                }
                // right turn or straight
                else if (orientation == 90) {

                    if (rand == 0) {

                        goal += 90;
                        goal = goal % 360;
                        rightTurn = transform.eulerAngles.y;
                    }
                    else rand = 2;
                }
                // straight only
                else rand = 2;
            }
            else rand = -1;
        }
        else if (Vector3.Distance(intersections[2], transform.position) < neighborhood && !decisionMade) {

            lastIntersection = intersections[2];
            decisionMade = true;
            audioPlaying = false;

            // right turn only
            if (orientation == 90) {

                rand = 0;
                goal += 90;
                goal %= 360;
                rightTurn = transform.eulerAngles.y;
            }
            // left turn only
            else {

                rand = 1;
                if (goal == 0) goal = 270;
                else goal -= 90;
                goal = goal % 360;
                leftTurn = transform.eulerAngles.y;
            }
        }
        else if (Vector3.Distance(intersections[3], transform.position) < neighborhood && !decisionMade) {

            if (((orientation == 0 || orientation == 180) && northGreen) || ((orientation == 90 || orientation == 270) && eastGreen) && !yellow) {

                rand = Mathf.RoundToInt(Random.Range(0, 2));

                lastIntersection = intersections[3];
                decisionMade = true;
                audioPlaying = false;

                // right or left
                if (orientation == 270) {
                    if (rand == 0) {

                        goal += 90;
                        goal = goal % 360;
                        rightTurn = transform.eulerAngles.y;
                    }
                    else if (rand == 1) {

                        if (goal == 0) goal = 270;
                        else goal -= 90;
                        goal = goal % 360;
                        leftTurn = transform.eulerAngles.y;
                    }
                }
                // right turn or straight
                else if (orientation == 0) {

                    if (rand == 0) {

                        goal += 90;
                        goal = goal % 360;
                        rightTurn = transform.eulerAngles.y;
                    }
                    else rand = 2;
                }
                // straight only
                else rand = 2;
            }
            else rand = -1;
        }
        else if (Vector3.Distance(intersections[4], transform.position) < neighborhood && !decisionMade) {

            if (((orientation == 0 || orientation == 180) && northGreen) || ((orientation == 90 || orientation == 270) && eastGreen) && !yellow) {

                rand = Mathf.RoundToInt(Random.Range(0, 2));

                lastIntersection = intersections[4];
                decisionMade = true;
                audioPlaying = false;

                // right or straight from every direction
                if (rand == 0) {

                    goal += 90;
                    goal = goal % 360;
                    rightTurn = transform.eulerAngles.y;
                }
                else rand = 2;
            }
            else rand = -1;
        }
        else if (Vector3.Distance(intersections[5], transform.position) < neighborhood && !decisionMade) {

            if (((orientation == 0 || orientation == 180) && northGreen) || ((orientation == 90 || orientation == 270) && eastGreen) && !yellow) {

                rand = Mathf.RoundToInt(Random.Range(0, 2));

                lastIntersection = intersections[5];
                decisionMade = true;
                audioPlaying = false;

                // right or left
                if (orientation == 90) {
                    if (rand == 0) {

                        goal += 90;
                        goal = goal % 360;
                        rightTurn = transform.eulerAngles.y;
                    }
                    else if (rand == 1) {

                        if (goal == 0) goal = 270;
                        else goal -= 90;
                        goal = goal % 360;
                        leftTurn = transform.eulerAngles.y;
                    }
                }
                // right turn or straight
                else if (orientation == 180) {

                    if (rand == 0) {

                        goal += 90;
                        goal = goal % 360;
                        rightTurn = transform.eulerAngles.y;
                    }
                    else rand = 2;
                }
                // straight only
                else rand = 2;
            }
            else rand = -1;
        }
        if (Vector3.Distance(intersections[6], transform.position) < neighborhood && !decisionMade) {

            lastIntersection = intersections[6];
            decisionMade = true;
            audioPlaying = false;

            // right turn only
            if (orientation == 270) {

                rand = 0;
                goal += 90;
                goal %= 360;
                rightTurn = transform.eulerAngles.y;
            }
            // left turn only
            else {

                rand = 1;
                if (goal == 0) goal = 270;
                else goal -= 90;
                goal = goal % 360;
                leftTurn = transform.eulerAngles.y;
            }
        }
        else if (Vector3.Distance(intersections[7], transform.position) < neighborhood && !decisionMade) {

            if (((orientation == 0 || orientation == 180) && northGreen) || ((orientation == 90 || orientation == 270) && eastGreen) && !yellow) {

                rand = Mathf.RoundToInt(Random.Range(0, 2));

                lastIntersection = intersections[7];
                decisionMade = true;
                audioPlaying = false;

                // right or left
                if (orientation == 180) {
                    if (rand == 0) {

                        goal += 90;
                        goal = goal % 360;
                        rightTurn = transform.eulerAngles.y;
                    }
                    else if (rand == 1) {

                        if (goal == 0) goal = 270;
                        else goal -= 90;
                        goal = goal % 360;
                        leftTurn = transform.eulerAngles.y;
                    }
                }
                // right turn or straight
                else if (orientation == 270) {

                    if (rand == 0) {

                        goal += 90;
                        goal = goal % 360;
                        rightTurn = transform.eulerAngles.y;
                    }
                    else rand = 2;
                }
                // straight only
                else rand = 2;
            }
            else rand = -1;
        }
        else if (Vector3.Distance(intersections[8], transform.position) < neighborhood && !decisionMade) {

            lastIntersection = intersections[8];
            decisionMade = true;
            audioPlaying = false;

            // right turn only
            if (orientation == 180) {

                rand = 0;
                goal += 90;
                goal %= 360;
                rightTurn = transform.eulerAngles.y;
            }
            // left turn only
            else {

                rand = 1;
                if (goal == 0) goal = 270;
                else goal -= 90;
                goal = goal % 360;
                leftTurn = transform.eulerAngles.y;
            }
        }

        // does movement based on the random number
        if (!CarInFront()) {
            if (rand == 0) RightTurn();
            else if (rand == 1) LeftTurn();
            else if (rand == 2) GoForward();
            else WaitForLight();
        }

        // when far enough away from last intersection, allow turns again.
        if (Vector3.Distance(transform.position, lastIntersection) > (neighborhood + 3.0f)) decisionMade = false;
    }

    void RightTurn() {

        if (audioPlaying) {
            audio.GetComponent<AICarAudio>().PlayMoving();
            audioPlaying = true;
        }

        if (goal == 0) {

            goal = 360;
        }

        if (rightTurn < goal) {

            transform.Rotate(0, rightTurnRotation, 0);
            rightTurn += rightTurnRotation;
        }

        // set actual angle to be exacly goal, should keep cars from straying from lines
        else {

            transform.eulerAngles = new Vector3(0, goal, 0);
        }

        transform.position += (transform.forward * -1) * speed;
    }

    void GoForward() {

        if (audioPlaying) {
            audio.GetComponent<AICarAudio>().PlayMoving();
            audioPlaying = true;
        }

        transform.position += (transform.forward * -1) * speed;
    }

    void LeftTurn() {

        if (audioPlaying) {
            audio.GetComponent<AICarAudio>().PlayMoving();
            audioPlaying = true;
        }

        // needed because 0 is never > than 270
        if (leftTurn == 0) {

            leftTurn = 360;
        }

        if (leftTurn > goal) {

            transform.Rotate(0, leftTurnRotation, 0);
            leftTurn += leftTurnRotation;
        }

        // set actual angle to be exacly goal, should keep cars from straying from lines
        else {

            transform.eulerAngles = new Vector3(0, goal, 0);
        }

        transform.position += (transform.forward * -1) * speed;
    }

    void WaitForLight() {

        if (audioPlaying) {
            audio.GetComponent<AICarAudio>().PlayIdle();
            audioPlaying = true;
        }
    }

    bool CarInFront() {

        for (int i = 0; i < cars.Length; i++) {

            // I spent like 3 hours on this. I know its disgusting but I couldn't figure out another way to do it
            // this makes sure the two objects are close and facing the same direction
            if (Vector3.Angle(this.transform.forward, cars[i].transform.forward) == 0 && Vector3.Distance(this.transform.position, cars[i].transform.position) < 8) {
                // needed this because both cars would stop, rather than just the back one
                // check the orientation then check that the car is indeed behind the other
                if (Mathf.RoundToInt(this.transform.eulerAngles.y) == 0 && this.transform.position.z > cars[i].transform.position.z) return true;
                if (Mathf.RoundToInt(this.transform.eulerAngles.y) == 90 && this.transform.position.x > cars[i].transform.position.x) return true;
                if (Mathf.RoundToInt(this.transform.eulerAngles.y) == 180 && this.transform.position.z < cars[i].transform.position.z) return true;
                if (Mathf.RoundToInt(this.transform.eulerAngles.y) == 270 && this.transform.position.x < cars[i].transform.position.x) return true;

            }
        }

        return false;
    }
}
