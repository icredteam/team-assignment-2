﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour
{
    public float greenTime; // Duration of green light
    public float yellowTime; // Duration of yellow light

    public GameObject northRed; // Lights on the streetlight
    public GameObject northYellow;
    public GameObject northGreen;
    public GameObject eastRed;
    public GameObject eastYellow;
    public GameObject eastGreen;
    public GameObject southRed;
    public GameObject southYellow;
    public GameObject southGreen;
    public GameObject westRed;
    public GameObject westYellow;
    public GameObject westGreen;

    private float timeToChange; // Time until the next light change

    void Start()
    {

        timeToChange = greenTime;
    }

    void Update()
    {
        timeToChange -= Time.deltaTime;
        if (timeToChange <= 0)
        {
            ChangeLights();
            SetTime();
        }
    }

    void SetTime()
    {
        if ((northGreen != null) && northGreen.activeInHierarchy || (eastGreen != null) && eastGreen.activeInHierarchy)
        {
            timeToChange = greenTime;
        }
        else
        {
            timeToChange = yellowTime;
        }
    }

    void ChangeLights()
    {
        if ((northGreen != null) &&northGreen.activeInHierarchy) // North-South is green
        {
            northGreen.SetActive(false); // Set North-South to yellow
            southGreen.SetActive(false);
            northYellow.SetActive(true);
            southYellow.SetActive(true);
        }
        else if ((eastGreen != null) && eastGreen.activeInHierarchy) // East-West is green
        {
            eastGreen.SetActive(false); // Set East-West to yellow
            westGreen.SetActive(false);
            eastYellow.SetActive(true);
            westYellow.SetActive(true);
        }
        else if ((northYellow != null) && northYellow.activeInHierarchy) // North-South is yellow
        {
            northYellow.SetActive(false); // Set North-South to red, and East-West to green
            southYellow.SetActive(false);
            northRed.SetActive(true);
            southRed.SetActive(true);
            eastRed.SetActive(false);
            westRed.SetActive(false);
            eastGreen.SetActive(true);
            westGreen.SetActive(true);
        }
        else if ((eastYellow != null) && eastYellow.activeInHierarchy) // East-West is yellow
        {
            eastYellow.SetActive(false); // Set East-West to red, and North-South to green
            westYellow.SetActive(false);
            eastRed.SetActive(true);
            westRed.SetActive(true);
            northRed.SetActive(false);
            southRed.SetActive(false);
            northGreen.SetActive(true);
            southGreen.SetActive(true);
        }
    }

    public bool NorthSouthIsGreen()
    {
        return northGreen.activeInHierarchy;
    }

    public bool EastWestIsGreen()
    {
        return eastGreen.activeInHierarchy;
    }
}
