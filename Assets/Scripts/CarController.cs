﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
public class CarController : MonoBehaviour {

    public GameObject wheel;
    public Valve.VR.SteamVR_ActionSet m_actionSet;
    private Valve.VR.SteamVR_Input_Sources inputSources;
    public Valve.VR.SteamVR_Action_Boolean TriggerClick;
    public Valve.VR.SteamVR_Action_Boolean Brake;
    public Valve.VR.SteamVR_Action_Boolean Grip;
    public float currentSpeed;
    public float maxSpeed = 5;
    public float speedDiff = 0.01F;
    public float slowDiff = 0.005F;
    bool trig = false;
    bool brake = false;
    bool grip = false;

    private bool playingIdle, playingMoving;
    public GameObject carAudio;
    // Use this for initialization
    void Start() {
        m_actionSet.Activate(Valve.VR.SteamVR_Input_Sources.Any, 0, true);
    }
    private void Awake() {
        TriggerClick.AddOnStateDownListener(TriggerDown, inputSources);
        TriggerClick.AddOnStateUpListener(TriggerUp, inputSources);
        Brake.AddOnStateDownListener(BrakeDown, inputSources);
        Brake.AddOnStateUpListener(BrakeUp, inputSources);
        Grip.AddOnStateDownListener(GripUp, inputSources);
        Grip.AddOnStateUpListener(GripUp, inputSources);
    }
    // Update is called once per frame
    void Update() {
        Debug.Log(Mathf.Abs(this.transform.localRotation.z - wheel.transform.localRotation.z));
      //  Quaternion rot = Quaternion.Lerp(transform.rotation, wheel.transform.localRotation, Time.deltaTime * 0.5F);
       // this.transform.Rotate(0, rot.z, 0);
        if (trig || grip)
        {
            float z = (this.transform.localRotation.z - wheel.transform.localRotation.z) * -1;
            this.transform.Rotate(0, z, 0);
        }
        transform.position += (transform.forward * -1) * currentSpeed;
        if (brake) {
            if (currentSpeed >= 0) {
                currentSpeed -= slowDiff * 5;
                if (!playingIdle) {
                    carAudio.GetComponent<PlayerCarAudio>().PlayIdle();
                    playingIdle = true;
                    playingMoving = false;
                }
            }
        }
        if (trig == true) {
            if (currentSpeed < maxSpeed) {
                currentSpeed += speedDiff;
                if (!playingMoving) {
                    carAudio.GetComponent<PlayerCarAudio>().PlayMoving();
                    playingIdle = false;
                    playingMoving = true;
                }
            }
        }
        else {
            if (currentSpeed >= 0) {
                currentSpeed -= slowDiff;
                if (!playingIdle) {
                    carAudio.GetComponent<PlayerCarAudio>().PlayIdle();
                    playingIdle = true;
                    playingMoving = false;
                }
            }

        }
        if (currentSpeed < 0) {
            currentSpeed = 0;
        }
    }

    private void TriggerDown(Valve.VR.SteamVR_Action_Boolean fromAction, Valve.VR.SteamVR_Input_Sources fromSource) {
        trig = true;
    }
    private void TriggerUp(Valve.VR.SteamVR_Action_Boolean fromAction, Valve.VR.SteamVR_Input_Sources fromSource) {
        trig = false;
    }
    private void BrakeDown(Valve.VR.SteamVR_Action_Boolean fromAction, Valve.VR.SteamVR_Input_Sources fromSource) {
        brake = true;
    }
    private void BrakeUp(Valve.VR.SteamVR_Action_Boolean fromAction, Valve.VR.SteamVR_Input_Sources fromSource) {
        brake = false;
    }
    private void GripUp(Valve.VR.SteamVR_Action_Boolean fromAction, Valve.VR.SteamVR_Input_Sources fromSource)
    {
        grip = true;
    }
    private void GripDown(Valve.VR.SteamVR_Action_Boolean fromAction, Valve.VR.SteamVR_Input_Sources fromSource)
    {
        grip = true;
    }
}
