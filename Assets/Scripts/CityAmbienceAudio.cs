﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityAmbienceAudio : MonoBehaviour {

    public AudioClip fireTruck, honk, police, train, dogs;
    private float time;

    private AudioSource source;

    void Awake() {
        source = GetComponent<AudioSource>();
        source.enabled = true;
        time = Time.time;
    }

    void Update() {

        if (Time.time > time + Random.Range(10, 20)) {

            int r = Mathf.RoundToInt(Random.Range(0, 4));

            if (r == 0) PlayFireTruck();
            else if (r == 1) PlayHonk();
            else if (r == 2) PlayPolice();
            else if (r == 3) PlayTrain();
            else PlayDog();
        }
    }

    public void PlayFireTruck() {
        source.clip = fireTruck;
        source.Play();
        time = Time.time;
    }

    public void PlayHonk() {
        source.clip = honk;
        source.Play();
        time = Time.time;
    }

    public void PlayPolice() {
        source.clip = police;
        source.Play();
        time = Time.time;
    }

    public void PlayDog() {
        source.clip = dogs;
        source.Play();
        time = Time.time;
    }

    public void PlayTrain() {
        source.clip = train;
        source.Play();
        time = Time.time;
    }
}
