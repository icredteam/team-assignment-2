﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AICarAudio : MonoBehaviour {

    public AudioClip moving, idle;

    private AudioSource source;

    void Awake() {
        source = GetComponent<AudioSource>();
        source.enabled = true;
    }

    public void PlayMoving() {
        source.clip = moving;
        source.Play();
    }

    public void PlayIdle() {
        source.clip = idle;
        source.Play();
    }
}
