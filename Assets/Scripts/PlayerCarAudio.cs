﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerCarAudio : MonoBehaviour {

    public AudioClip idle, moving;
    private AudioSource source;

    void Awake() {
        source = GetComponent<AudioSource>();
        source.enabled = true;
    }

    public void PlayMoving() {
        source.clip = moving;
        source.Play();
    }

    public void PlayIdle() {
        source.clip = idle;
        source.Play();
    }
}
